const express = require("express");
const Character = require("../models/Character");


const router = express.Router();

router.get("/", (req, res) => {
  res
    .status(200)
    .json("Welcome to the (probably) best Ninja Turtle Database off the World");
});


router.get("/characters", async (req, res, next) => {
  try {
    const characters = await Character.find();
    return res.status(200).json(characters);
  } catch (err) {
    return next(err);
  }
});

router.post("/create", async (req, res, next) => {
 try {
   /**
   * req.params --> 
   * req.query --> 
   * req.body --> 
   */

   /**
    * Para guardar nuevos datos en la base dedatos,
    * 1. Creamos instancia del modelo
    * 2. Ejecutamos isntancia.save() de la instancia
    * 3. Hacemos lo que queramos con el dato.
    */
  const newCharacter = new Character({
      name: req.body.name,
      mask: req.body.mask,
      weapon: req.body.weapon,
      roll: req.body.roll,
      cool: req.body.cool,
      eaten_pizzas: req.body.eaten_pizzas,
      race: req.body.race,
  })

 await newCharacter.save();

  console.log('Personaje creado')

  return res.status(200).json('Character creado')

 } catch (error) {
    next(error)
 }
  


 
})

router.put('/edit/:id', async(req, res, next) => {
  try {
    const { id } = req.params;
    const characterModify = new Character(req.body)
    characterModify._id = id;                            /* { new: true } devuelve el personaje actualizado */
    const characterUpdated = await Character.findByIdAndUpdate(id, characterModify, { new: true });
    return res.status(200).json({message: `${characterUpdated.name} updated succesfully`, data: characterUpdated})
    } catch (err) {
    return next(err)
  }
})

router.delete(('/:id'), async (req, res, next) => {
  try {
    const { id } = req.params;

    const deleted = await Character.findByIdAndDelete(id);
    if (deleted) {
      return res.status(200).json({ message: 'Character deleted', data: deleted })
    } else {
      return res.status(404).json("Can't find character")
    }
  } catch (err) {
    next(err)  
  }
})

router.get("/characters/:id", async (req, res, next) => {
  try {
    const id = req.params.id;
    const character = await Character.findById(id);

    if (character) {
      return res.status(200).json(character);
    } else {
      return res.status(404).json(`No se encuentra el personaje con id: ${id}`);
    }
  } catch (err) {
    return next(err);
  }
});

router.get("/characters/name/:name", async (req, res, next) => {
  try {
    const { name } = req.params;
    const character = await Character.find({
      name: {
        $regex: new RegExp("^" + name.toLowerCase(), "i"),
      },
    });
    console.log("character--->", character);
    return res.status(200).json(character);
  } catch (err) {
    return next(err);
  }
});

router.get("/characters/race/:race", async (req, res, next) => {
  try {
    const { race } = req.params;
    const character = await Character.find({
      race: {
        $regex: new RegExp("^" + race.toLowerCase(), "i"),
      },
    });
    // añadir un .toLowerCase para que funcione con el case insensitive de arriba
    if (character && race.toLowerCase() == "turtle") {
      // Para poder enviar un mensaje en el JSON juntos con los datos debemos hacer un objeto dentro con message: y data:
      return res.status(200).json({ message: "THERE ARE ALL YOUR TURTLES", data: character });
    } else if (character && race.toLowerCase() == "rat") {
      return res.status(200).json({ message: "YES... A RAT, LIKE YOU", data: character });
    } else if (character && race.toLowerCase() == "human") {
      return res.status(200).json({ message: "BOORING HUMANS...", data: character });
    } else if (character && race.toLowerCase() == "weird") {
      return res.status(200).json({ message: "DISGUSTING LIFE FORMS ...", data: character });
    } else {
      return res.status(404).json("You're so rare...");
    }
  } catch (err) {
    return next(err);
  }
});

router.get("/characters/eaten_pizzas/:eaten_pizzas", async (req, res, next) => {
  try {
    const { eaten_pizzas } = req.params;
    const character = await Character.find({
      eaten_pizzas: { $gte: eaten_pizzas },
    });
    return res.status(200).json(character);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;

