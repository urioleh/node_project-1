const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const filmSchema = new Schema(
    {
        title: {type: String},
        director: {type: String},
        year: {type: Number},
        characters: [{type: mongoose.Types.ObjectId, ref: 'Character'}]
    }                   /**Schema.Types.ObjectId es lo mismo */
    ,
    {
        timestamps: true,
    }
);


const Film = mongoose.model('Films', filmSchema);
module.exports = Film;