const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// Creamos el esquema de personajes
const characterSchema = new Schema(
    {
        name: {type: String, required: true},
        mask: {type: String},
        weapon: {type: String, required: true},
        roll: {type: String, required: true},
        cool: {type: Boolean, default: true},
        eaten_pizzas: {type: Number},
        race: {type: String, enum: ['Rat', 'Turtle','Human', 'Weird'], required: true},
        
    },
    {
        timestamps:true ,
    }
);

const Character = mongoose.model('Character' , characterSchema);
module.exports = Character;