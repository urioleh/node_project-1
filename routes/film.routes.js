const express = require('express');
const Film = require('../models/Films');

const router = express.Router();

router.get('/', async (req, res, next) => {
    try {    /* .populate('propiedad del Schema base', '(opcional)propeidades que requieres del Schema referido' ) */
        const films = await Film.find().populate('characters', 'name mask weapon race');
        return res.status(200).json(films)
    } catch (error) {
        return next(error);
    }
})

router.delete('/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        const deleted = await Film.findByIdAndDelete(id);
        if (deleted) {
            return res.status(200).json({ message: 'Character deleted', data: deleted })
          } else {
            return res.status(404).json("Can't find character")
          }
    } catch (error) {
        return next(error);
    }
})

router.post('/create', async (req, res, next) => {
    try {
        const { title, director, year, characters } = req.body;
        const newFilm = new Film({title, director, year, characters })
        const createdFilm = await newFilm.save()
        return res.status(200).json(createdFilm)
    } catch (error) {
        return next(error)
    }
})

router.put('/add-Character-to-Film'), async (req, res, next) => {
    try {
        const { filmId } = req.body;
        const { characterId } = req.body;
        const updatedFilm = await Film.findByIdAndUpdate(
            filmId,
            // $push añade aunque ya exista uno igual.
            // $addToSet añade si no existe.
            { $addToSet: {characters: characterId} },
            { new: true }
        );
        return res.status(200).json(updatedFilm)    
    } catch (error) {
       return next(error)
    }
}

module.exports = router;