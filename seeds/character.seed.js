const mongoose = require('mongoose');
const Character = require('../models/Character');
const {DB_URL, CONFIG_DB} = require('../utils/db')

console.log(DB_URL)
console.log(CONFIG_DB)

const charactersArray = [ 
    {
        name: "Leonardo",
        mask: "Blue",
        weapon: "Two ninjato",
        roll: "Lider",
        race: "Turtle" ,
        eaten_pizzas: "3"
    },
    {
        name: 'Raphael',
        mask: 'Red',
        weapon: 'Two sai',
        roll: 'Warrior',
        race: 'Turtle' ,
        eaten_pizzas: '6'
    },
    {
        name: 'Michelangelo',
        mask: 'Yellow',
        weapon: 'Nunchaku',
        roll: 'Joker',
        race: 'Turtle' ,
        eaten_pizzas: '5'
    },
    {
        name: "Donatello",
        mask: "Purple",
        weapon: "Stick",
        roll: "Sciencist",
        race: "Turtle" ,
        eaten_pizzas: "2"
    },
    {
        name: 'Splinter',
        weapon: 'Nails',
        roll: 'Master',
        race: 'Rat' ,
        eaten_pizzas: '1'
    },
]

mongoose.connect(DB_URL, CONFIG_DB)
.then(async() => {
    console.log(`Ejecutando seed de Characters...`)
    const allCharacters = await Character.find();

    if(allCharacters.length) {
        await Character.collection.drop();
    }
})
.catch(err => console.log(`Error buscando en la DB`, err))
.then(async() => {
    await Character.insertMany(charactersArray);
    console.log(`Añadidos nuevos personajes a DB`)
})
.catch(err => console.log('Error añadiendo los personajes', err))
.finally(() => mongoose.disconnect());

