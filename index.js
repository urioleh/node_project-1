const express = require('express')
const {connectToDb} = require('./utils/db')
const characterRouter = require('./routes/character.routes')
const filmRouter = require('./routes/film.routes')

connectToDb()

const PORT = 3000;

const server = express()


// Esta linea le dice a Express, que si recibe un (POST, PUT..) con un JSON adjunto, lo lea
// y nos lo mande al endpoint dentro de req.body:
server.use(express.json());
// Si la petición (POST, PUT...) y viene en formato form-urlencoded, meta la información en req.body;
server.use(express.urlencoded({ extended: false }));

server.use('/', characterRouter);
server.use('/film', filmRouter);

// Controlador de Errores (error handler)
// Única función de express que lleva como primer argumento un error.
server.use((error, req, res, next) => {
// al usar next(), la ejecución llega a este punto.
// si no usamos next(), nunca llegaremos hasta aquí.
    const status = error.status || 500;
    const message = error.message || 'Unexpected error! La que has liao pollito'

    return res.status(status).json(message);
})

const serverCallback = () => console.log(`Servidor funcionando en localhost:${PORT}`)

server.listen(PORT, serverCallback)